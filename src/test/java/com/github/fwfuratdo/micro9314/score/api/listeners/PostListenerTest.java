package com.github.fwfuratdo.micro9314.score.api.listeners;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.only;

//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE)
//@AutoConfigureStubRunner(ids = "com.github.fwfurtado.micro9314:posts:+:9090", stubsMode = StubRunnerProperties.StubsMode.LOCAL)
class PostListenerTest {

    @Autowired
    private StubTrigger trigger;

    @SpyBean
    private PostListener listener;

    @Captor
    private ArgumentCaptor<PostListener.CreatedPost> argumentCaptor;

    @Test
    @Disabled
    void test() {
        trigger.trigger("create_post");

        then(listener).should(only()).handle(argumentCaptor.capture());

        var event = argumentCaptor.getValue();

        assertEquals("Some title", event.title());
        assertEquals(1, event.id());
        assertEquals(1, event.userId());
    }

}