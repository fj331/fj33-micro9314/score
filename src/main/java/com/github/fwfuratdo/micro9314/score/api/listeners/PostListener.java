package com.github.fwfuratdo.micro9314.score.api.listeners;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

@Component
public class PostListener {

    record CreatedPost(
            @JsonProperty String title,
            @JsonProperty Long id,
            @JsonProperty Long userId
    ){
        @JsonCreator
        static CreatedPost of(String title, Long id, Long userId) {
            return new CreatedPost(title, id, userId);
        }
    }


    @StreamListener(Sink.INPUT)
    void handle(CreatedPost event) {
        System.out.println(event);
    }

}
