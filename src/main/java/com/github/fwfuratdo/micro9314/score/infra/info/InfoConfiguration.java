package com.github.fwfuratdo.micro9314.score.infra.info;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class InfoConfiguration implements InfoContributor {

    private final SomeInfo info;

    public InfoConfiguration(SomeInfo info) {
        this.info = info;
    }

    @Override
    public void contribute(Info.Builder builder) {
        builder.withDetail("current_value", info.getValue());
    }

    @RefreshScope
    @Component
    static class SomeInfo {

        @Value("${app.values:'N/A'}")
        private String value;


        public String getValue() {
            return value;
        }
    }
}
