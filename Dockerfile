FROM adoptopenjdk/openjdk14:debian-slim

ARG JAR

COPY ${JAR} /app/application.jar

WORKDIR /app

CMD ["java", "--enable-preview", "-jar", "application.jar"]


